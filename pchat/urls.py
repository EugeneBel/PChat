from django.conf.urls import include, url
from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import reverse


def redirect(request):
    return HttpResponseRedirect(reverse('chat:index'))

urlpatterns = [
    url(r'^$', redirect),
    url(r'^chat/', include('chat.urls')),
    url(r'^admin/', admin.site.urls),
]
