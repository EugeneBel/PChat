from django.db import models


class Messages(models.Model):
    message_text = models.CharField(max_length=500)
    post_date = models.DateTimeField('date posted')

    def __str__(self):
        return self.message_text