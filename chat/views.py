import json
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseForbidden
from django.template import loader
from django.urls import reverse
from django.utils import timezone
from chat.models import Messages


def get_list_latest_messages():
    return reversed(Messages.objects.filter(
        post_date__lte=timezone.now()
    ).order_by('-post_date')[:15])


def index(request):
    if request.user.is_authenticated():
        template = loader.get_template('chat/index.html')
        context = {
            'latest_messages_list': get_list_latest_messages(),
            'user_name': request.user,
        }
        return HttpResponse(template.render(context, request))
    else:
        return HttpResponseRedirect(reverse('chat:login'))


def send(request):
    try:
        message = Messages(message_text=request.POST['message'], post_date=timezone.now())
    except (KeyError, Messages.DoesNotExist):
        return HttpResponse('<h1><font color="red">Miss message</font></h1>')
    message.save()
    return HttpResponseRedirect(reverse('chat:index'))


def get_messages(request):
    list_messages = get_list_latest_messages()
    array_messages = []
    for message in list_messages:
        array_messages.append(message.message_text)
    return HttpResponse(json.dumps(list(array_messages)), content_type="application/json")


def sigh_in(request):
    try:
        login_str = request.POST['login']
        password_str = request.POST['password']
        if str(login_str.strip()) == '' and str(password_str.strip()) == '':
            raise KeyError
    except KeyError:
        return HttpResponseForbidden('<h1><font color="red">Miss login/password</font></h1>')

    user = authenticate(username=login_str, password=password_str)
    if user is not None:
        if user.is_active:
            login(request, user)
            return HttpResponseRedirect(reverse('chat:index'))
        else:
            return HttpResponseForbidden('<h1><font color="red">Account disabled</font></h1>')
    else:
        if User.objects.filter(username=login_str).exists():
            return HttpResponseForbidden('<h1><font color="red">Wrong password</font></h1>')
        else:
            user = User.objects.create_user(login_str, '', password_str)
            user.save()
            login(request, user)
            return HttpResponseRedirect(reverse('chat:index'))


def login_site(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('chat:index'))
    else:
        template = loader.get_template('chat/login.html')
        return HttpResponse(template.render({}, request))


def sigh_out(request):
    logout(request)
    return HttpResponseRedirect(reverse('chat:index'))