from django.conf.urls import url

from . import views

app_name = 'chat'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^send/$', views.send, name='send'),
    url(r'^messages/$', views.get_messages, name='messages'),
    url(r'^sighin/$', views.sigh_in, name='sighin'),
    url(r'^sighout/$', views.sigh_out, name='sighout'),
    url(r'^login/$', views.login_site, name='login'),
]